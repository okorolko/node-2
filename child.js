process.on('message', (msg) => {
  const delay = Math.random() * 1000;

  setTimeout(() => {
    process.send(msg);

  }, delay)
});