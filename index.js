const { fork } = require('child_process');
const numCPUs = require('os').cpus().length;

const CPUsArray = [...Array(numCPUs).keys()];

const forkedPromises = CPUsArray
                        .map(initForkedProcesses)
                        .map(sendMessage)
                        .map(getForkedPromise)

Promise.all(forkedPromises).then(process.exit).catch(console.log);

function initForkedProcesses() {
  return fork('child.js');
}

function sendMessage(forked) {
  forked.send('hello world!');
  return forked;
}

function getForkedPromise(forked, i) {
  return new Promise((resolve, reject) => {
    forked.on('message', (msg) => {
      console.log(`Message from child ${i + 1}`, msg);
      resolve(msg);
    });
  })
}
